﻿namespace WindowsOperation.Design
{
    using System;
    using System.Activities.Presentation.Metadata;
    using System.ComponentModel;
    using System.Drawing;

    // Логика взаимодействия для WindowsOperationDesigner.xaml
    public partial class WindowsOperationDesigner
    {
        public WindowsOperationDesigner()
        {
            this.InitializeComponent();
        }

        public static void RegisterMetadata(AttributeTableBuilder builder)
        {
            builder.AddCustomAttributes(
                typeof(Alefair.WindowsOperation.WindowsOperation),
                new DesignerAttribute(typeof(WindowsOperationDesigner)),
                new DescriptionAttribute("Windows Operation Activity"),
                new ToolboxBitmapAttribute(typeof(Alefair.WindowsOperation.WindowsOperation), "LOGO.png"));
        }
    }
}
