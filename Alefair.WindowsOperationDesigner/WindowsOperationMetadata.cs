﻿namespace WindowsOperation.Design
{
    using System.Activities.Presentation.Metadata;

    public sealed class WindowsOperationDesignerMetadata : IRegisterMetadata
    {

        public static void RegisterAll()
        {
            var builder = new AttributeTableBuilder();
            WindowsOperationDesigner.RegisterMetadata(builder);

            // TODO: Other activities can be added here
            MetadataStore.AddAttributeTable(builder.CreateTable());
        }
        public void Register()
        {
            RegisterAll();
        }
    }
}
