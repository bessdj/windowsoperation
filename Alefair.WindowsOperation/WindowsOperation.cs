﻿namespace Alefair.WindowsOperation
{
    using System;
    using System.Text;
    using System.Activities;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Activities.Presentation.PropertyEditing;

    public class WindowsOperation : CodeActivity
    {

        public enum operation
        {
            WinActive,
            WinClose,
            WinExists,
            WinGetHandle,
            WinGetPos,
            WinGetProcess,
            WinGetTitle,
            WinSetTitle
        }

        private IDictionary<string, InArgument> Arguments;

        [Category("Input")]
        [Description("Select function to use")]
        [RequiredArgument]
        public operation OperationName { get; set; }

        [Category("Input")]
        [Description("Dictionary")]
        ///[DisplayName("Dictionary")]
        [Browsable(true)]
        [Editor(typeof(Alefair.WindowsOperation.Editors.DynamicArgumentDictionaryEditor), typeof(PropertyValueEditor))]
        public IDictionary<string, InArgument> Parameters
        {
            get
            {
                if (this.Arguments == null)
                {
                    this.Arguments = new Dictionary<string, InArgument>();
                }
                return this.Arguments;
            }
        }

        [Category("Output")]
        public OutArgument<object> Result { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            int iArgCount = Arguments.Count;

            switch (OperationName)
            {
                case operation.WinActive:
                    Console.WriteLine("Case 1");
                    break;
                case operation.WinClose:
                    Console.WriteLine("Case 2");
                    break;
                case operation.WinExists:
                    Console.WriteLine("Case 2");
                    break;
                case operation.WinGetHandle:
                    Console.WriteLine("Case 2");
                    break;
                case operation.WinGetPos:
                    Console.WriteLine("Case 2");
                    break;
                case operation.WinGetProcess:
                    Console.WriteLine("Case 2");
                    break;
                case operation.WinGetTitle:
                    Console.WriteLine("Case 2");
                    break;
                case operation.WinSetTitle:
                    Console.WriteLine("Case 2");
                    break;
                default:
                    Console.WriteLine("Default case");
                    break;
            }

            Result.Set(context, Arguments);
        }
    }
}
