﻿using System;
using System.Linq;
using System.Activities;
using System.Activities.Presentation;
using System.Activities.Presentation.Model;
using System.Activities.Presentation.Converters;
using System.Activities.Presentation.PropertyEditing;
using System.Windows;
using System.Reflection;
using System.Xml.Linq;
using Microsoft.VisualBasic.Activities;

namespace Alefair.WindowsOperation.Editors
{
    public class DynamicArgumentDictionaryEditor : DialogPropertyValueEditor
    {
        public DynamicArgumentDictionaryEditor()
        {
            Type type = Type.GetType("System.Activities.Core.Presentation.Themes.EditorCategoryTemplateDictionary, System.Activities.Core.Presentation, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35");
            Uri resourceLocator = new Uri(type.Assembly.GetName().Name + ";component/System/Activities/Core/Presentation/Themes/EditorCategoryTemplateDictionary.xaml", UriKind.RelativeOrAbsolute);
            ResourceDictionary resources = (ResourceDictionary)Application.LoadComponent(resourceLocator);
            base.InlineEditorTemplate = resources["ArgumentCollection_InlineTemplate"] as DataTemplate;
        }

        public override void ShowDialog(PropertyValue propertyValue, IInputElement commandSource)
        {
            ModelPropertyEntryToOwnerActivityConverter converter = new ModelPropertyEntryToOwnerActivityConverter();
            ModelItem activity = (ModelItem)converter.Convert(propertyValue.ParentProperty, typeof(ModelItem), false, null);
            ModelItem item2 = (ModelItem)converter.Convert(propertyValue.ParentProperty, typeof(ModelItem), true, null);

            //workaround: EditingContext context = ((IModelTreeItem)activity).ModelTreeManager.Context;
            Type type = Type.GetType("System.Activities.Presentation.Model.IModelTreeItem, System.Activities.Presentation, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35");
            ModelTreeManager manager = (ModelTreeManager)type.GetProperties().FirstOrDefault(e => e.Name == "ModelTreeManager").GetValue(activity, null);
            EditingContext context = (EditingContext)typeof(ModelTreeManager).GetProperties(BindingFlags.Instance | BindingFlags.NonPublic).FirstOrDefault(e => e.Name == "Context").GetValue(manager, null);

            ModelItemDictionary dictionary = item2.Properties[propertyValue.ParentProperty.PropertyName].Dictionary;
            DynamicArgumentDesignerOptions options = new DynamicArgumentDesignerOptions()
            {
                Title = propertyValue.ParentProperty.DisplayName
            };
            if (DynamicArgumentDialog.ShowDialog(activity, dictionary, context, activity.View, options))
            {

            }
        }

    }
}
